
document.getElementById('btnFind').onclick = function() {
    chrome.tabs.query({currentWindow: true, active: true}, function (tabs){
        sendMsg(tabs[0]);
    });
};

function sendMsg (activeTabId) {
    chrome.tabs.sendMessage(activeTabId.id, {findSeries: true});
}